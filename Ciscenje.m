function cis = Ciscenje(bw,udeoH, udeoV)

%% Odstranjanje manjih komponenti
% 
% Ulazni parametri:
%
% bw    - binarna slika
% udeo  - procentualni deo koji treba ocistiti
%
% Izlazni parametri
%
% cis   - binarna slika bez sumva


%% Definisanje podrazumevanih vrednosti

if nargin < 2
    udeoH = 0.01;
    udeoV = 0.05;
end

%% Ciscenje slike

f1 = bw;
f2 = f1;
f1sum = sum(f1');

for i =1:length(f1sum)
    if (f1sum(i)<udeoH*max(f1sum))
        f2(i,:) = 0;
    end
end

f3 = f2;
f2sum = sum(f2);

for i =1:length(f2sum)
    if (f2sum(i)<udeoV*max(f2sum))
        f3(:,i) = 0;
    end
end

cis = f3;

end