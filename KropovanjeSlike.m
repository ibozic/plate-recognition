function cropI = KropovanjeSlike(I,bw, r, k);
%% Funkcija ucitava binarnu sliku u oblika tablice, a kao izlaz vraca kropovanu originalnu sliku automobilske tablice
% 
% Ulazni parametri:
%
% I     - originalna slika u nijansama sivog
% bw    - binarna slika oblika tablice
% r     - prazan prostor iznad i ispod tablice
% k     - prazan prostor levo i desno od tablice 
%
% Izlazni parametri
%
% cropI - Kropovana originalna slika u nivoima sivog

%% Definisanje podrazumevanih vrednosti

if nargin < 4
    k = 10;
end


if nargin < 3
    r = 30;
    k = 30;
end


%% Odredjivanje poyicija za kropovanje

colBW = sum(bw);
rowBW = sum(bw');

for i = 1:length(colBW)-k
    if (colBW(i) == 0 && colBW(i+k) ~=0)
        minCol = i;
    end
    if (colBW(i) ~= 0 && colBW(i+k) ==0)
        maxCol = i;
    end
end

for i = 1:length(rowBW)-r
    if (rowBW(i) == 0 && rowBW(i+r) ~=0)
        minRow = i;
    end
    if (rowBW(i) ~= 0 && rowBW(i+r) ==0)
        maxRow = i;
    end
end

%% Kropovanje originalne slike

cropI = I((minRow-r):(maxRow+r),(minCol-k):(maxCol+k));

figure(4)
imshow(cropI)

end