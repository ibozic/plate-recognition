function conBW = Komponente(bw,pov,equal)

%% Funkcija ucitava binarnu sliku, a vraca najvecu ili sv sem najvece 
%  poveyanu komponentu
% 
% Ulazni parametri:
%
% bw    - binarna slika
% pov   - kriterijum poveyanosti piksela
% equal - parametar koji odrdjuje da li trazimo sve sem najvece (true)
%       - ili sam najveci povezani region (false)
%
% Izlazni parametri
%
% conBW - binarna slika poveyanih komponentilogicka slika rama tablice


%% Definisanje podrazumevanih vrednosti

if nargin < 3
    equal = false;
end

if nargin < 2
    equal = false;
    pov   = 8;
end
    
%% Odredjivanje tipa poveyanih komponenti

 CC = bwconncomp(bw,pov);
 numPixels = cellfun(@numel,CC.PixelIdxList);
 [biggest,idx] = max(numPixels);
 
 if (equal)
     for i = 1:length(numPixels)
         if (i==idx)
             bw(CC.PixelIdxList{i}) = 0;
         end
     end
 else
     for i = 1:length(numPixels)
         if (i~=idx)
             bw(CC.PixelIdxList{i}) = 0;
         end
     end
 end

 conBW = bw;
end