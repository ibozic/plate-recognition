function [linije tacke] = odredivanjeLinijaTemena(regI)

%% Funkcija ucitava binarnu sliku registracija i vraca dve matrice, 
%  u jednoj su sacuvane vrednosti linija, a u drugoj temena registracije
% 
% Ulazni parametri:
%
% regI   - binarna slika registarskog dela
%
% Izlazni parametri
%
% linije - granicne linije frejma registracije
% tacke  - temena frejma registracije 

%% Predobrada i odredjivanje Hafove transformacije

thinLP = bwmorph(pureLP, 'thin',Inf);


[H, theta, rho] = hough(thinLP);
countTheta = 0;
k = 1;
[mH, nH] = size(H);

%% Odredjivanje maksimuma Hafoive transformacije


while(countTheta<2)
    thetaM = max(H);
    [maxTheta, iTheta] = max(thetaM);
    rhoM = max(H');
    [maxRho, iRho] = max(rhoM);
    p(k,1) = rho(iRho);
    p(k,2) = theta(iTheta);
    m(k) = H(iRho, iTheta);
    H(iRho, iTheta) = 0;
    if (abs(theta(iTheta))<20)
        countTheta = countTheta + 1;
    end
    k = k+1;
end
    
%% Odredjivanje granicnih linija

lenP = length(p);
j = 1;
for i = 1:lenP
    if (j<3)
        linija(j,1)=p(i,1);
        linija(j,2)=p(i,2);
        j = j+1;
    end
    if (j>2 && abs(p(i,2))<20)
        linija(j,1)=p(i,1);
        linija(j,2)=p(i,2);
        j = j+1;
    end
end

%% Prikaz linija

l1x = 1:mL;
l1y = linija(1,1)/sin(degtorad(linija(1,2))) - l1x .* (cos(degtorad(linija(1,2)))/sin(degtorad(linija(1,2))));
l2y = linija(2,1)/sin(degtorad(linija(2,2))) - l1x .* (cos(degtorad(linija(2,2)))/sin(degtorad(linija(2,2))));
l3y = linija(3,1)/sin(degtorad(linija(3,2))) - l1x .* (cos(degtorad(linija(3,2)))/sin(degtorad(linija(3,2))));
l4y = linija(4,1)/sin(degtorad(linija(4,2))) - l1x .* (cos(degtorad(linija(4,2)))/sin(degtorad(linija(4,2))));

figure(1)
imshow(regI); hold on;
plot(l1x,l1y,'r','LineWidth',3); hold on;
plot(l1x,l2y,'g','LineWidth',3); hold on;
plot(l1x,l3y,'b','LineWidth',3); hold on;
plot(l1x,l4y,'m','LineWidth',3); hold off;

%% Odredjivanje i prikaz tacaka

t1 = [cos(degtorad(linija(1,2))) sin(degtorad(linija(1,2))); cos(degtorad(linija(3,2))) sin(degtorad(linija(3,2)))]\[linija(1,1); linija(3,1)];
t2 = [cos(degtorad(linija(1,2))) sin(degtorad(linija(1,2))); cos(degtorad(linija(4,2))) sin(degtorad(linija(4,2)))]\[linija(1,1); linija(4,1)];
t3 = [cos(degtorad(linija(2,2))) sin(degtorad(linija(2,2))); cos(degtorad(linija(4,2))) sin(degtorad(linija(4,2)))]\[linija(2,1); linija(4,1)];
t4 = [cos(degtorad(linija(2,2))) sin(degtorad(linija(2,2))); cos(degtorad(linija(3,2))) sin(degtorad(linija(3,2)))]\[linija(2,1); linija(3,1)];

figure(2)
imshow(regI); hold on;
plot(round(t1(1,1)),round(t1(2,1)),'y*','LineWidth',5); hold on;
plot(round(t2(1,1)),round(t2(2,1)),'y*','LineWidth',5); hold on;
plot(round(t3(1,1)),round(t3(2,1)),'y*','LineWidth',5); hold on;
plot(round(t4(1,1)),round(t4(2,1)),'y*','LineWidth',5); hold off;


tacke = [t1 t2 t3 t4];

end

