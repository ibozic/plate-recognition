%% Ucitavanje slike i konverzija u sliku nijansi sivog

I = imread('2.jpg');
I = rgb2gray(I);

%% Odredjivanje oblika tablice

bw = OblikTablice(I);

conBW = Komponente(bw);

%% Izdvajanje tablice sa celokupne slike

cropI = KropovanjeSlike(I,conBW);

%% Odredjivanje dela sa registracijom

bw1 = OblikTablice(cropI);

conBW1 = Komponente(bw1);

% reg = Registracija(conBW1);
% 
% cropI1 = KropovanjeSlike(cropI,conBW1);
% 
% %% Odredjivanje linija i tacaka okvira tablice
% 
% [linije tacke] = odredivanjeLinijaTemena(reg);

%% Odredjivanje karaktera

bw2 = OblikTablice(cropI);

conBW2 = Komponente(bw2,8,true);

conBW2 = Ciscenje(conBW2);

[opstina, broj, oznaka] = deljenjeSlike(conBW2);

Opstina;

Broj;

Oznaka;