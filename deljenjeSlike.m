function [opstina, broj, oznaka] = deljenjeSlike(bw)

%% funkcija uzima binarnu sliku sa registracijom i vraca pojedinacne komponente
% 
% Ulazni parametri:
%
% bw       - binarna slika
%
% Izlazni parametri
%
% opstina - binarna slika sa delom rgistracije koji dredjuje opstinu
% broj    - binarna slika sa delom rgistracije koji sadrzi registracioni broj
% oznaka  - binarna slika sa delom rgistracije koji dredjuje oznaku

%% Deljenje slike
[m,n] = size(bw);

opstina = bw(round(0.05*m):round(0.95*m),round(n*.12):round(n*.35));
broj    = bw(round(0.05*m):round(0.95*m),round(n*.42):round(n*.72));
oznaka  = bw(round(0.05*m):round(0.95*m),round(n*.75):end);


end