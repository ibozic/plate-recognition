function reg = Registracija(fcI)

%% Funkcija ucitava binarnu sliku kropovanog frejma tablice i vraca frejm tablice na kojem se nalazi registracija
% 
% Ulazni parametri:
%
% fcI  - binarna slika kropovanog frejma tablice
%
% Izlazni parametri
%
% reg  - binarna slika sa delom tablice sa registracijom

%% Odredjivanje watrshed transformacije ulazne slike

L = watershed(fcI,8);
[nL, mL]=size(L);

%% Odredjivanje dominantne watershad komponente

maxL = max(max(L));
elL = zeros(1,maxL);
for k = 2:maxL
    suma = 0;
    for i = 1:nL
        for j = 1:mL
            if (L(i,j) == k)
                suma = suma + 1;
            end
        end
    end
    elL(k) = suma;
end

[c,g] = max(elL);

%% Odredjivanje potpune povrsine dela sa rgistracijom

L1 = zeros(nL,mL);
    for i = 1:nL
        for j = 1:mL
            if (L(i,j) == g)
                L1(i,j) = 1;
            end
        end
    end
    
%% Odredjivanje okvira registracione tablice

reg = L1.*bwmorph(fcI,'dilate');

figure(5)
imshow(reg)

end
