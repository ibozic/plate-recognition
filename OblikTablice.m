function bw = OblikTablice(I,prag,bin,med)

%% Funkcija ucitava sliku u sivim nijansama, a kao izlaz vraca ram automobilske tablice
% 
% Ulazni parametri:
%
% I    - slika u nijansama sivog
% prag - prag prilikom odredjivanja vertikalnih ivica
% bin  - prag prilikom binarizacije slike
% med  - vrednost koriscena kod medijan filtra
%
% Izlazni parametri
%
% bw   - logicka slika rama tablice

%% Definisanje podrazumevanih vrednosti

if nargin < 4
    med = 7;
end


if nargin < 3
    bin = 240;
    med = 7;
end

if nargin < 2
    prag =  55;
    bin  = 240;  
    med  =   7;
end

c = size(I)
if (size(c,2)==3)
    error('Nije ucitana slika u nijansama sivog');
end

%% Odredjivanje vertikalnih ivica

k = [-1 0 1; -2 0 2; -1 0 1];

H = conv2(double(I),k, 'same');
V = conv2(double(I),k','same');
E = sqrt(H.*H + V.*V);
vEdge = uint8((E > prag) * 255);

figure (1);
imshow(vEdge)

%% Binarizacija slike

[m,n] = size(vEdge)
bw = ones(m,n);
for i = 1:m
    for j=1:n
        if (vEdge(i,j)<bin)
            bw(i,j)=0;
        end
    end
end

figure (2);
imshow(bw)

%% Odredjivanje mediana zarad uklanjanja impulsng suma

bw = median(bw,med);

end
